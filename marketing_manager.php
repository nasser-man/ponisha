<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

include_once (__DIR__  ."/base_model.php");
include_once (__DIR__  ."/marketing_groups_model.php");
include_once (__DIR__  ."/pricing_locations_model.php");
include_once (__DIR__  ."/registered_users_model.php");
include_once (__DIR__  ."/users_groups_model.php");

date_default_timezone_set('Asia/Tehran');

// -----------------------------------------------------------------------------

function return_json_response($data , $http_response_code = 200){
    header("Content-Type: application/json;charset=utf-8");
    http_response_code($http_response_code);
    echo json_encode($data);
}

// -----------------------------------------------------------------------------

function search(){
    $order_by = "";
    switch ($_POST["order_by"]) {
        case 'group_name':
            $order_by = "ORDER BY " .Marketing_groups_model::TABLE_NAME. ".group_name ASC ";
            break;
        case 'date_modified':
            $order_by = "ORDER BY " .Marketing_groups_model::TABLE_NAME. ".date_modified DESC ";
            break;
        case 'date_modified':
            $order_by = "ORDER BY tedade_userha DESC ";
            break;

        default:
            break;
    }
    $search_params = [
        "page_size"=>intval($_POST["page_size"]),
        "page_index"=>intval($_POST["page_index"]),
        "group_name" => $_POST["search_query"],
        "order_by"=> $order_by
    ];
    $total = 0;
    $result = Marketing_groups_model::FIND($search_params , $total);

    $rows = "";
    foreach($result as $row){
        $_tr = "<tr >".
            "<td style='text-align:center;'><a uk-icon=\"icon: plus-circle\" onclick='return false;' href='#'></a></td>".
            "<td style='text-align:center;'><button uk-icon=\"icon: pencil\" onclick='edit_group(".$row->PK().");'></button></td>".
            "<td>".$row->group_name."</td>".
            "<td>".$row->date_modified."</td>".
            "<td>".$row->tedade_userha."</td>".
            "</tr>";
        $rows .= $_tr;
    }

    $response = [
        "total"=>$total,
        "result"=>$rows
    ];
    echo json_encode($response);
}

// -----------------------------------------------------------------------------

function group($id_group = -1)
{
    $response = [];
    $group = new Marketing_groups_model();
    if($id_group > 0){
        $group->load($id_group);
    }
    $response["group"] = $group;

    $response["countries"] = Pricing_locations_model::FIND([
        "order_by"=>"ORDER BY country_name",
        "page_size"=>"100"
    ]);

    $_countries = [];
    foreach($response["countries"] as $country){
        $_countries[$country->country_code] = $country->country_name;
    }

    $response['userhaye_group'] = [];
    if($group->PK() > 0){
        $response['userhaye_group'] = $group->list_userhaye_group();
        foreach($response['userhaye_group'] as $user){
            $user->country_name = $_countries[$user->country_code];
        }
    }


    return_json_response($response);
}

// -----------------------------------------------------------------------------

function save_group()
{
    $id_group = intval($_POST["group_id"]);
    $group = new Marketing_groups_model();
    if($id_group > 0){
        $group->load($id_group);
    }
    $group->group_name = $_POST["group_name"];
    $group->date_modified = time();
    $group->description = $_POST["group_description"];

    $validate_group = $group->validate();
    if($validate_group["code"] < 1){
        return_json_response($validate_group);
        return;
    }
    $group->save();

    $users = [];

    $tbl_user_id = $_POST["tbl-user-id"];
    $tbl_user_username = $_POST["tbl-user-username"];
    $tbl_user_email = $_POST["tbl-user-email"];
    $tbl_user_country_code = $_POST["tbl-user-country_code"];

    $id_userha_dar_group = [];

    for($i = 0 ; $i < count($tbl_user_id) ; $i++){
        $user = new Registered_users_model();
        $_id_user = intval($tbl_user_id[$i]);
        if($_id_user > 0){
            $user->load($_id_user);
        }
        $user->username = $tbl_user_username[$i];
        $user->email = $tbl_user_email[$i];
        $user->country_code = $tbl_user_country_code[$i];

        $v = $user->validate();
        if($v["code"] < 1){
            echo $v["code"] . " : " . $v["message"];
            continue;
        } else {
            $user->save();
        }


        if(!users_groups_model::IS_USER_IN_GROUP($user->PK() , $group->PK())){
            $user_group = new Users_groups_model();
            $user_group->id_user = $user->PK();
            $user_group->id_group = $group->PK();
            $user_group->save();
        }
        $id_userha_dar_group[] = $user->PK();
    }
    Users_groups_model::HAZFE_USERHAYE_GROUP($group->PK() , $id_userha_dar_group);

    return_json_response([
        "code"=> 1,
        "message"=>"group saved",
        "group_id"=>$group->PK()
    ]);
}

// -----------------------------------------------------------------------------

function validate_user()
{
    $user = new Registered_users_model();
    $user->username = $_POST["username"];
    $user->email = $_POST["email"];
    $user->country_code = $_POST["country_code"];

    return_json_response($user->validate());
}

// -----------------------------------------------------------------------------

$action = $_GET["action"];
switch ($action) {
    case 'search':
        search();
        break;

    case 'group':
        $id_group = intval($_GET["id"]);
        group($id_group);
        break;

    case 'save_group':
        save_group();
        break;

    case 'validate_user':
        validate_user();
        break;

    default:
        # code...
        break;
}

?>
