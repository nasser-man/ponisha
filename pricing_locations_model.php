<?php

class Pricing_locations_model extends Base_model
{
    const TABLE_NAME = "pricing_locations";

    public $id;
    public $country_name;
    public $country_code;

    // -------------------------------------------------------------------------

    function __construct()
    {
        $this->id = -1;
        $this->country_code = $this->country_name = "";
    }

    // -------------------------------------------------------------------------

    public function load($id)
    {
        $sql = 'SELECT * FROM ' .Pricing_locations_model::TABLE_NAME. ' where `id` = ' . $id;
        $connection = $this->connect();
        mysql_select_db($this->get_db_name());
        $results = mysqli_query($connection,$sql);
        if(!$results){
            die('Could not get data: ' . mysqli_error());
        }
        $row = mysqli_fetch_assoc($results);
        if(!$row){
            return -1000;
        }
        $this->populate($row);
    }

    // -------------------------------------------------------------------------

    public function populate($row)
    {
        $this->id = intval($row["id"]);
        $this->country_name = $row["country_name"];
        $this->country_code = $row["country_code"];
    }

    // -------------------------------------------------------------------------
    /**
    * پارامترهایی را دریافت کرده بر اساس انها در دیتابیس جستجو می کند
    * @param [key=>value] params
    * @param & int $tedade_kole_natayej
    * @return [\Marketing_groups_model]
    */
    public static function FIND($params = [] , &$tedade_kole_natayej = 0)
    {
        if(!isset($params["id"]))   $params["id"] = -1;
        if(!isset($params["order_by"]))     $params["order_by"] = "ORDER BY id DESC";
        if(!isset($params["page_size"]))   $params["page_size"] = 20;
        if(!isset($params["page_index"]))   $params["page_index"] = 0;
        if(!isset($params["country_code"])) $params["country_code"] = "";
        if(!isset($params["country_name"])) $params["country_name"] = "";
        $return = [];

        $m = new Pricing_locations_model();
        $connection = $m->connect();

        $sql = 'Select * from '.Pricing_locations_model::TABLE_NAME.' ';
        $sql_count = 'select count(*) as count from '.Pricing_locations_model::TABLE_NAME.' ';
        if($params["id"] > 0){
            $sql .= " where `id` = " . $params["id"];
            $sql_count .= " where `id` = " . $params["id"];
        }
        if(strlen($params["country_code"]) > 0){
            $sql .= " where `country_code` = " . $params["country_code"];
            $sql_count .= " where `country_code` = " . $params["country_code"];
        }
        if(strlen($params["country_name"]) > 0){
            $sql .= " where `country_name` = " . $params["country_name"];
            $sql_count .= " where `country_name` = " . $params["country_name"];
        }

        $count_result = mysqli_query($connection , $sql_count );
        $_row = mysqli_fetch_assoc($count_result);
        $tedade_kole_natayej = intval($_row["count"]);

        $sql .= " " . $params["order_by"]." ";
        $sql .= " LIMIT " .$params["page_size"] . " OFFSET " . $params['page_size']*$params['page_index']." ";

        $result = mysqli_query($connection,$sql);
        while($row = mysqli_fetch_assoc($result)){
            $object = new Pricing_locations_model();
            $object->populate($row);
            $return[] = $object;
        }
        return $return;
    }

    // -------------------------------------------------------------------------
}
