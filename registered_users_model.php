<?php

class Registered_users_model extends Base_model
{
    const TABLE_NAME = "registered_users";

    public $id;
    public $username;
    public $email;
    public $registered_date;
    public $country_code;
    public $agent_status;
    public $detail;

    // -------------------------------------------------------------------------

    function __construct()
    {
        $this->id = -1;
        $this->username = "";
        $this->email = "";
        $this->registered_date = time();
        $this->country_code = "";
        $this->agent_status = -1;
        $this->detail = "";
    }

    // -------------------------------------------------------------------------

    public function load($id)
    {
        $rows = Registered_users_model::FIND([
            "id"=>$id,
            "page_size"=>1
        ]);
        if(count($rows) < 1){
            return -1000;
        }
        $row = array_shift($rows);
        $this->id = $row->PK();
        $this->username = $row->username;
        $this->email = $row->email;
        $this->registered_date = $row->registered_date;
        $this->country_code = $row->country_code;
        $this->agent_status = $row->agent_status;
        $this->detail = $row->detail;
    }

    // -------------------------------------------------------------------------

    public function validate()
    {
        if(strlen($this->username)  < 1){
            return [
                "code"=>-1 ,
                "message"=>"Please Enter Name of User."
            ];
        }
        if(strlen($this->email)  < 1){
            return [
                "code"=>-2 ,
                "message"=>"Please Enter Email of User."
            ];
        }
        if(!Registered_users_model::CHECK_EMAIL_IS_UNIQUE($this->email , $this->PK())){
            return [
                "code"=>-3 ,
                "message"=>"Entered Email is used by another user."
            ];
        }
        return [
            "code"=>1 ,
            "message"=>"OK"
        ];
    }

    // -------------------------------------------------------------------------

    public static function CHECK_EMAIL_IS_UNIQUE($email , $id_ignore_user = -1)
    {
        $r = Registered_users_model::FIND(["email"=>$email]);
        if(count($r) < 1){
            return true;
        }
        if(count($r) > 1){
            return false;
        }
        $first_row = array_shift($r);
        if($first_row->PK() === $id_ignore_user){
            return true;
        }
        return false;
    }

    // -------------------------------------------------------------------------

    public function populate($row)
    {
        $this->id = intval($row["id"]);
        $this->username = $row["username"];
        $this->email = $row["email"];
        $this->registered_date = intval($row["registered_date"]);
        $this->country_code = $row["country_code"];
        $this->agent_status = intval($row["agent_status"]);
        $this->detail = $row["detail"];
    }

    // -------------------------------------------------------------------------
    /**
    * پارامترهایی را دریافت کرده بر اساس انها در دیتابیس جستجو می کند
    * @param [key=>value] params
    * @param & int $tedade_kole_natayej
    * @return [\Marketing_groups_model]
    */
    public static function FIND($params = [] , &$tedade_kole_natayej = 0)
    {
        if(!isset($params["id"]))           $params["id"] = -1;
        if(!isset($params["username"]))     $params["username"] = "";
        if(!isset($params["order_by"]))     $params["order_by"] = "ORDER BY id DESC";
        if(!isset($params["page_size"]))    $params["page_size"] = 20;
        if(!isset($params["page_index"]))   $params["page_index"] = 0;
        if(!isset($params["country_code"])) $params["country_code"] = "";
        if(!isset($params["email"]))        $params["email"] = "";
        $return = [];

        $m = new Registered_users_model();
        $connection = $m->connect();

        $sql = 'Select * from '.Registered_users_model::TABLE_NAME.' where 1=1 ';
        $sql_count = 'select count(*) as count from '.Registered_users_model::TABLE_NAME.' where 1=1 ';
        if($params["id"] > 0){
            $sql .= " and `id` = " . $params["id"];
            $sql_count .= " and `id` = " . $params["id"];
        }
        if(strlen($params["username"]) > 0){
            $sql .= " and `username` like '" . $params["username"] ."'";
            $sql_count .= " and `username` like '" . $params["username"] ."'";
        }
        if(strlen($params["country_code"]) > 0){
            $sql .= " and `country_code` = " . $params["country_code"];
            $sql_count .= " and `country_code` = " . $params["country_code"];
        }
        if(strlen($params["email"]) > 0){
            $sql .= " and `email` like '" . $params["email"] ."'";
            $sql_count .= " and `email` like '" . $params["email"] ."'";
        }

        $count_result = mysqli_query($connection , $sql_count );
        $_row = mysqli_fetch_assoc($count_result);
        $tedade_kole_natayej = intval($_row["count"]);

        $sql .= " " . $params["order_by"]." ";
        $sql .= " LIMIT " .$params["page_size"] . " OFFSET " . $params['page_size']*$params['page_index']." ";

        $result = mysqli_query($connection,$sql);
        while($row = mysqli_fetch_assoc($result)){
            $object = new Registered_users_model();
            $object->populate($row);
            $return[] = $object;
        }
        return $return;
    }

    // -------------------------------------------------------------------------

    public function save()
    {
        $m = new Registered_users_model();
        $connection = $m->connect();

        $sql = "";
        if($this->PK() > 0){
            $sql = "UPDATE ".Registered_users_model::TABLE_NAME." SET ".
                "username='".$this->username."', ".
                "email='".$this->email."', ".
                "registered_date='".date('Y-m-d H:i:s',$this->registered_date)."', ".
                "country_code='".$this->country_code."', ".
                "agent_status=".$this->agent_status.", ".
                "detail='".$this->detail."' ".
                " WHERE id= " . $this->PK();
        } else {
            $sql = "INSERT INTO ".Registered_users_model::TABLE_NAME." (username , email , registered_date , country_code , agent_status , detail) ".
                "VALUES ('".$this->username."', '".$this->email."' , '".date('Y-m-d H:i:s',$this->registered_date)."', '".$this->country_code."' , ".$this->agent_status." , '".$this->detail."')";
        }

        $r = mysqli_query($connection , $sql );
        if ($r === TRUE) {
            if($this->PK() < 1){
                $this->id = mysqli_insert_id($connection);
            }
            $connection->close();
            return TRUE;
        } else {
            $connection->close();
            return FALSE;
        }
    }

    // -------------------------------------------------------------------------
}
