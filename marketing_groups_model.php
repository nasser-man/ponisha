<?php

class Marketing_groups_model extends Base_model
{
    const TABLE_NAME = "marketing_groups";

    public $id;
    public $group_name;
    public $date_modified;
    public $description;
    // public $full_name;
    // public $email;
    // public $country;
    // public $detail;
    // public $interest;

    // -------------------------------------------------------------------------

    function __construct()
    {
        $this->id = -1;
        $this->group_name = "";
        $this->date_modified = "";
        $this->description = "";
        // $this->full_name = $this->email = $this->country = $this->detail = "";
        // $this->interest = "";
    }

    // -------------------------------------------------------------------------

    public function load($id)
    {
        $rows = Marketing_groups_model::FIND([
            "id"=>$id,
            "page_size"=>1
        ]);
        if(count($rows) < 1){
            return -1000;
        }
        $row = array_shift($rows);
        $this->id = $row->PK();
        $this->group_name = $row->group_name;
        $this->date_modified = $row->date_modified;
        $this->description = $row->description;
    }

    // -------------------------------------------------------------------------

    public function validate()
    {
        if(strlen($this->group_name) < 1){
            return [
                "code"=>-1 ,
                "message"=>"Please enter name of group."
            ];
        }

        return [
            "code"=>1 ,
            "message"=> "OK"
        ];
    }

    // -------------------------------------------------------------------------

    public function populate($row)
    {
        $this->id = intval($row["id"]);
        $this->group_name = $row["group_name"];
        $this->date_modified = $row["date_modified"];
        $this->description = $row["description"];
        // $this->full_name = $row["full_name"];
        // $this->email = $row["email"];
        // $this->country = $row["country"];
        // $this->detail = $row["detail"];
        // $this->interest = $row["interest"];
    }

    // -------------------------------------------------------------------------
    /**
    * پارامترهایی را دریافت کرده بر اساس انها در دیتابیس جستجو می کند
    * @param [key=>value] params
    * @param & int $tedade_kole_natayej
    * @return [\Marketing_groups_model]
    */
    public static function FIND($params = [] , &$tedade_kole_natayej = 0)
    {
        if(!isset($params["id"]))   $params["id"] = -1;
        if(!isset($params["order_by"]))     $params["order_by"] = "ORDER BY id DESC";
        if(!isset($params["page_size"]))   $params["page_size"] = 20;
        if(!isset($params["page_index"]))   $params["page_index"] = 0;
        if(!isset($params["group_name"]))   $params["group_name"] = "";
        $return = [];

        $m = new Marketing_groups_model();
        $connection = $m->connect();

        $marketing_groups = Marketing_groups_model::TABLE_NAME;
        $users_groups = Users_groups_model::TABLE_NAME;

        $sql = "Select $marketing_groups.* , count($users_groups.id_user) as tedade_userha  from $marketing_groups ";
        $sql_count = "select count(*) as count from $marketing_groups where 1=1 ";
        $sql .= "LEFT JOIN $users_groups ON $marketing_groups.id = $users_groups.id_group where 1=1 ";
        if($params["id"] > 0){
            $sql .= " and $marketing_groups.`id` = " . $params["id"];
            $sql_count .= " and $marketing_groups.`id` = " . $params["id"];
        }
        if(strlen($params["group_name"]) > 0){
            $sql .= " and $marketing_groups.`group_name` like '%" . $params["group_name"] ."%' ";
            $sql_count .= " and $marketing_groups.`group_name` like '%" . $params["group_name"] ."%' ";
        }

        $count_result = mysqli_query($connection , $sql_count );
        $_row = mysqli_fetch_assoc($count_result);
        $tedade_kole_natayej = intval($_row["count"]);

        $sql .= " GROUP BY $marketing_groups.id ";
        $sql .= " " . $params["order_by"]." ";
        $sql .= " LIMIT " .$params["page_size"] . " OFFSET " . $params['page_size']*$params['page_index']." ";

        $result = mysqli_query($connection,$sql);
        while($row = mysqli_fetch_assoc($result)){
            $object = new Marketing_groups_model();
            $object->populate($row);
            $object->tedade_userha = $row['tedade_userha'];
            $return[] = $object;
        }
        return $return;
    }

    // -------------------------------------------------------------------------

    public function list_userhaye_group()
    {
        $i = Users_groups_model::FIND([
            "id_group" => $this->PK()
        ]);

        $list = [];
        foreach($i as $item){
            $user = new Registered_users_model();
            $user->load($item->id_user);
            $list[] = $user;
        }
        return $list;
    }

    // -------------------------------------------------------------------------

    public function save()
    {
        $m = new Marketing_groups_model();
        $connection = $m->connect();

        $sql = "";
        if($this->PK() > 0){
            $sql = "UPDATE ".Marketing_groups_model::TABLE_NAME." SET ".
                "group_name='".$this->group_name."', ".
                "date_modified='".date('Y-m-d H:i:s',$this->date_modified)."', ".
                "description='".$this->description."' ".
                " WHERE id= " . $this->PK();
        } else {
            $sql = "INSERT INTO ".Marketing_groups_model::TABLE_NAME." (group_name , date_modified , description) ".
                "VALUES ('".$this->group_name."', '".date('Y-m-d H:i:s',$this->date_modified)."', '".$this->description."')";
        }

        $r = mysqli_query($connection , $sql );
        if ($r === TRUE) {
            if($this->PK() < 1){
                $this->id = mysqli_insert_id($connection);
            }
            $connection->close();
            return TRUE;
        } else {
            $connection->close();
            return FALSE;
        }
    }

    // -------------------------------------------------------------------------

    public function tedade_userha()
    {
        $num = 0;
        $r = Users_groups_model::FIND(["fagat_tedad"=>TRUE] , $num);
        return $num;
    }

    // -------------------------------------------------------------------------
}
