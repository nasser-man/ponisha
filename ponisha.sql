-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2017 at 09:55 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.21-1~ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ponisha`
--

-- --------------------------------------------------------

--
-- Table structure for table `marketing_groups`
--

CREATE TABLE `marketing_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marketing_groups`
--

INSERT INTO `marketing_groups` (`id`, `group_name`, `date_modified`, `description`) VALUES
(4, 'my group 1', '2017-10-09 16:29:17', 'some descriptions about my group 1 / changed!'),
(5, 'my group 2 (updated)', '2017-10-09 18:23:41', 'some descriptions about my group 2 (updated)'),
(6, 'my group 3', '2017-10-09 16:41:15', 'some descriptions about my group 1 / changed!'),
(7, 'my group 4', '2017-10-09 16:42:54', 'some descriptions about my group 1 / changed!'),
(8, 'my group 2146', '2017-10-09 18:16:56', 'desc 2146');

-- --------------------------------------------------------

--
-- Table structure for table `pricing_locations`
--

CREATE TABLE `pricing_locations` (
  `id` int(11) NOT NULL,
  `country_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `country_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registered_users`
--

CREATE TABLE `registered_users` (
  `id` int(11) NOT NULL,
  `username` varchar(300) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci NOT NULL,
  `registered_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `country_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `agent_status` int(11) NOT NULL DEFAULT '-1',
  `detail` varchar(1000) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `marketing_groups`
--
ALTER TABLE `marketing_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricing_locations`
--
ALTER TABLE `pricing_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registered_users`
--
ALTER TABLE `registered_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `marketing_groups`
--
ALTER TABLE `marketing_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pricing_locations`
--
ALTER TABLE `pricing_locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registered_users`
--
ALTER TABLE `registered_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
