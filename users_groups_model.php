<?php

class Users_groups_model extends Base_model
{
    const TABLE_NAME = "users_groups";

    public $id;
    public $id_user;
    public $id_group;

    // -------------------------------------------------------------------------

    function __construct()
    {
        $this->id = -1;
        $this->id_user = $this->id_group = -1 ;
    }

    // -------------------------------------------------------------------------

    public function load($id)
    {
        $rows = Users_groups_model::FIND([
            "id"=>$id,
            "page_size"=>1
        ]);
        if(count($rows) < 1){
            return -1000;
        }
        $row = array_shift($rows);
        $this->id = $row->PK();
        $this->id_user = $row->id_user;
        $this->id_group = $row->id_group;
    }

    // -------------------------------------------------------------------------

    public function populate($row)
    {
        $this->id = intval($row["id"]);
        $this->id_user = $row["id_user"];
        $this->id_group = $row["id_group"];
    }

    // -------------------------------------------------------------------------
    /**
    * پارامترهایی را دریافت کرده بر اساس انها در دیتابیس جستجو می کند
    * @param [key=>value] params
    * @param & int $tedade_kole_natayej
    * @return [\Users_groups_model]
    */
    public static function FIND($params = [] , &$tedade_kole_natayej = 0)
    {
        if(!isset($params["id"]))           $params["id"] = -1;
        if(!isset($params["id_user"]))      $params["id_user"] = -1;
        if(!isset($params["id_group"]))     $params["id_group"] = -1;
        if(!isset($params["fagat_tedad"]))  $params["fagat_tedad"] = false;
        $return = [];

        $m = new Users_groups_model();
        $connection = $m->connect();

        $sql = 'Select * from '.Users_groups_model::TABLE_NAME.' where 1=1 ';
        $sql_count = 'select count(*) as c from '.Users_groups_model::TABLE_NAME.'  where 1=1';

        if($params["id"] > 0){
            $sql .= " and `id` = " . $params["id"];
            $sql_count .= " and `id` = " . $params["id"];
        }
        if($params["id_user"] > 0){
            $sql .= " and `id_user` = " . $params["id_user"];
            $sql_count .= " and `id_user` = " . $params["id_user"];
        }
        if($params["id_group"] > 0){
            $sql .= " and `id_group` = " . $params["id_group"];
            $sql_count .= " and `id_group` = " . $params["id_group"];
        }

        $count_result = mysqli_query($connection , $sql_count );
        $_row = mysqli_fetch_assoc($count_result);
        $tedade_kole_natayej = intval($_row["c"]);

        if(!$params["fagat_tedad"]){
            $result = mysqli_query($connection,$sql);
            while($row = mysqli_fetch_assoc($result)){
                $object = new Users_groups_model();
                $object->populate($row);
                $return[] = $object;
            }
        }
        return $return;
    }

    // -------------------------------------------------------------------------

    public static function IS_USER_IN_GROUP($id_user , $id_group)
    {
        $c = 0;
        $r = Users_groups_model::FIND(["id_user"=>$id_user , "id_group"=>$id_group , "fagat_tedad"=>true] , $c);
        if($c > 0){
            return TRUE;
        }
        return FALSE;
    }

    // -------------------------------------------------------------------------

    public static function HAZFE_USERHAYE_GROUP($id_group , $liste_id_userhaye_ignore=[])
    {
        $m = new Users_groups_model();
        $connection = $m->connect();

        $sql = 'DELETE FROM '.Users_groups_model::TABLE_NAME.' WHERE id_group = '.$id_group.' and id_user NOT IN (-1000';

        foreach($liste_id_userhaye_ignore as $_id){
            $sql .= ",".$_id;
        }
        $sql .= ");";

        // echo $sql;
        //
        // var_dump($liste_id_userhaye_ignore);

        $query_result = mysqli_query($connection , $sql );

        $connection->close();
    }

    // -------------------------------------------------------------------------

    public function save()
    {
        $m = new Users_groups_model();
        $connection = $m->connect();

        $sql = "";
        if($this->PK() > 0){
            $sql = "UPDATE ".Users_groups_model::TABLE_NAME." SET ".
                "id_user= ".$this->username.", ".
                "id_group= ".$this->username." ".
                " WHERE id= " . $this->PK();
        } else {
            $sql = "INSERT INTO ".Users_groups_model::TABLE_NAME." (id_user , id_group) ".
                "VALUES (".$this->id_user.", ".$this->id_group.")";
        }

        $r = mysqli_query($connection , $sql );
        if ($r === TRUE) {
            if($this->PK() < 1){
                $this->id = mysqli_insert_id($connection);
            }
            $connection->close();
            return TRUE;
        } else {
            $connection->close();
            return FALSE;
        }
    }

    // -------------------------------------------------------------------------
}
