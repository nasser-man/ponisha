<?php
class Base_model
{
    private $database_host = "127.0.0.1";
    private $database_user = "root";
    private $database_password = "1234";
    private $database_connection;
    private $database_name = "ponisha";

    // -------------------------------------------------------------------------

    public function connect(){
        $this->database_connection = mysqli_connect(
            $this->database_host,
            $this->database_user,
            $this->database_password,
            $this->database_name);
        if(!$this->database_connection){
            return null;
        }
        return $this->database_connection;
    }

    // -------------------------------------------------------------------------

    public function get_db_name(){
        return $this->database_name;
    }

    // -------------------------------------------------------------------------

    public function PK(){
        return $this->id;
    }

    // -------------------------------------------------------------------------

    public function validate(){
        return [
            "code"=>1 ,
            "message" => "OK"
        ];
    }

    // -------------------------------------------------------------------------
}
