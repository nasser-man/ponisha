var search_by = "id";

// -----------------------------------------------------------------------------

$(document).ready(function () {
    $("#result-table").freezeHeader();
    setTimeout(function(){ search(); }, 1000);
    $("#uk-search-groups-input").val("")
});

// -----------------------------------------------------------------------------

function search(){
    $("#result-table tbody").html("Loading result...");
    var q = $.ajax({
        url : "marketing_manager.php?action=search",
        data : {
            "page_size" : 20,
            "page_index" : 0,
            "search_query" : $("#uk-search-groups-input").val(),
            "order_by":search_by
        },
        type : "POST" ,
        dataType : "json"
    });

    q.fail(function(){
        alert("failed");
    });

    q.done(function(data){
        $("#result-table tbody").html(data.result);
        $("#result-table").multiSelect({
            actcls: 'selected-row',
            selector: 'tbody tr',
            except: ['tbody'],
            statics: ['.danger', '[data-no="1"]']
        });
    });
}

// -----------------------------------------------------------------------------

function groups_table_rows_selection_changed(){
    setTimeout(function(){
        var r1 = $("#result-table tbody tr").not(".selected-row");
        $.each(r1 , function(index , item){
            $(item).find("td:first").html("<a uk-icon=\"icon: plus-circle\" onclick='return false;' href='#'></a>");
        });
    } , 100);

    setTimeout(function(){
        var r2 = $("#result-table tbody tr.selected-row");
        $.each(r2 , function(index , item){
            $(item).find("td:first").html("<a uk-icon=\"icon: minus-circle\" onclick='return false;' href='#'></a>");
        });
    } , 200);
}

// -----------------------------------------------------------------------------

function save_group(){
    var form_id = "form-edit-group";
    var form_data = $("#"+form_id).serializeArray();

    var q = $.ajax({
        url : "marketing_manager.php?action=save_group",
        data : form_data ,
        type : "post" ,
        dataType : "json"
    });

    q.fail(function(){
        alert("Saving group data failed! #1843");
    });

    q.done(function(data){
        if(data.code > 0){
            UIkit.notification({
                message : data.message,
                status : "success"
            });
            search();
            var modal = $("#modal-container");
            var id_group = $(modal).find("#group-id");
            id_group.val(data.group_id);
            UIkit.modal(modal).hide();
        } else {
            UIkit.notification({
                message : data.message,
                status : "warning"
            });
        }
    });
}

// -----------------------------------------------------------------------------

function edit_group(id_group){
    if(id_group >0 ){
        $("#modal-header").html("Edit Group");
    } else {
        $("#modal-header").html("New Group");
    }

    var modal = $("#modal-container");

    var q = $.ajax({
        url : "marketing_manager.php?action=group&id="+id_group,
        data : {},
        type : "get" ,
        dataType : "json"
    });

    q.fail(function(){
        alert("failed #2130");
    });

    q.done(function(data){
        $(modal).find("#group-id").val(data.group.id);
        $(modal).find("#group_name").val(data.group.group_name);
        $(modal).find("#group_description").val(data.group.description);

        var select_country = $("#user-country");
        $("#user-country option").remove();
        $.each(data.countries ,function(index , item){
            var op = "<option value='"+item.country_code+"'>"+item.country_name+"</option>"
            $(select_country).append(op);
        });

        userhaye_group = data.userhaye_group;
        var users_table = $("#users-table tbody");
        $("#users-table tbody tr").remove();
        $.each(data.userhaye_group , function(index , item){
            var tr = "<tr>"+
                "<input type='hidden' name='tbl-user-id[]' id='tbl-user-id' value='"+item.id+"' />"+
                "<input type='hidden' name='tbl-user-username[]' id='tbl-user-username' value='"+item.username+"' />"+
                "<input type='hidden' name='tbl-user-email[]' id='tbl-user-email' value='"+item.email+"' />"+
                "<input type='hidden' name='tbl-user-country_code[]' id='tbl-user-country_code' value='"+item.country_code+"' />"+
                "<td></td>"+
                "<td></td>"+
                "<td>"+item.username+"</td>"+
                "<td>"+item.email+"</td>"+
                "<td>"+item.country_name+"</td>"+
            "</tr/>";
            users_table.append(tr);
        });

        UIkit.modal(modal).show();
    });
}

// -----------------------------------------------------------------------------

function submit_new_user(){
    var user_name = $("#user-name").val();
    var user_email = $("#user-email").val();
    var user_country_code = $("#user-country").val();

    var break_function = false;
    $.each($("#users-table tbody tr #user-email") , function(index , item){
        if(user_email == $(item).val()){
            UIkit.notification({
                message : "Another user with same email has been added.",
                status : "warning"
            });
            break_function = true;
            return;
        }
    });

    if(break_function){
        return;
    }


    var q = $.ajax({
        url : "marketing_manager.php?action=validate_user",
        data : {
            username : user_name,
            email : user_email,
            country_code : user_country_code
        },
        type : "post" ,
        dataType : "json"
    });

    q.fail(function(){
        alert("failed #1014");
    });

    q.done(function(data){
        if(data.code > 0){
            var new_user = [];
            new_user["username"] = $("#user-name").val();
            new_user["email"] = $("#user-email").val();
            new_user["country_code"] = $("#user-country").val();
            new_user["country_name"] = $('#user-country option:selected').text();
            var tr = "<tr>"+
                "<input type='hidden' name='tbl-user-id[]' id='tbl-user-id' value='-1' />"+
                "<input type='hidden' name='tbl-user-username[]' id='tbl-user-username' value='"+new_user["username"]+"' />"+
                "<input type='hidden' name='tbl-user-email[]' id='tbl-user-email' value='"+new_user["email"]+"' />"+
                "<input type='hidden' name='tbl-user-country_code[]' id='tbl-user-country_code' value='"+new_user["country_code"]+"' />"+
                "<td></td>"+
                "<td></td>"+
                "<td>"+new_user["username"]+"</td>"+
                "<td>"+new_user["email"]+"</td>"+
                "<td>"+new_user["country_name"]+"</td>"+
            "</tr/>";
            $("#users-table tbody").append(tr);

            $("#user-name").val("");
            $("#user-email").val("");
        } else {
            UIkit.notification({
                message : data.message,
                status : "warning"
            });
        }
    });
}

// -----------------------------------------------------------------------------
