<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

include_once (__DIR__  ."/base_model.php");
include_once (__DIR__  ."/marketing_groups_model.php");

?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/uikit.rtl.css" />
        <link rel="stylesheet" href="css/marketing.css" />
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.js"></script>
        <script src="js/jquery.freezeheader.js"></script>
        <script src="js/multiselect.js"></script>
        <script src="js/marketing.js"></script>
    </head>
    <body>

        <div id="main-content-div" class="main-content-div">

            <ul class="uk-flex-left"  uk-switcher="animation: uk-animation-fade" uk-tab>
                <li class="uk-active"><a href="#">Groups</a></li>
                <li><a href="#">Schedules</a></li>
                <li><a href="#">Templates</a></li>
                <li><a href="#">Compaigns</a></li>
                <li><a href="#">Reports</a></li>
            </ul>

            <ul class="uk-switcher uk-margin">
                <li>
                    <div class="tab-groups-container-div">
                        <nav class="uk-navbar-container" uk-navbar>
                            <div class="uk-navbar-left">
                                <div class="uk-button-group">
                                    <button class="uk-button uk-button-primary" onclick="edit_group(-1);" >Define new group</button>
                                    <button class="uk-button uk-button-primary">Sort...</button>
                                    <div uk-dropdown="mode: click">
                                        <div class="uk-dropdown-grid uk-child-width-1-2@m" uk-grid>
                                            <div>
                                                <ul class="uk-nav uk-dropdown-nav">
                                                    <li><button class="uk-button uk-button-text" onclick="search_by = 'group_name'; search();">Group name</button></li>
                                                    <li><button class="uk-button uk-button-text" onclick="search_by = 'date_modified'; search();">Date modified</button></li>
                                                    <li><button class="uk-button uk-button-text" onclick="search_by = 'tedade_userha'; search();">Number of members</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="uk-button uk-button-primary" type="button">...</button>
                                    <div uk-dropdown="mode: click">
                                        <div class="uk-dropdown-grid uk-child-width-1-2@m" uk-grid>
                                            <div>
                                                <ul class="uk-nav uk-dropdown-nav">
                                                    <li><a href="#">Edit</a></li>
                                                    <li><a href="#">Delete</a></li>
                                                    <li><a href="#">Merge</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-navbar-right">
                                <div class="search-bar-container">
                                    <form class="uk-search uk-search-default" onsubmit="search();return false;">
                                        <span uk-search-icon></span>
                                        <input class="uk-search-input" id="uk-search-groups-input" type="search" placeholder="Search..." value="">
                                    </form>
                                </div>
                            </div>
                        </nav>
                        <div class="uk-grid">
                            <div class=" uk-width-1-1">
                                <table id="result-table" class="uk-table uk-table-hover uk-table-divider" >
                                    <thead>
                                        <tr>
                                            <th style="text-align:center;"><span uk-icon="icon: tag"></th>
                                            <th style="text-align:center;"><span uk-icon="icon: pencil"></th>
                                            <th>Group Name</th>
                                            <th>Date Modified</th>
                                            <th>Number Of Members</th>
                                        </tr>
                                    </thead>
                                    <tbody onmousedown="groups_table_rows_selection_changed();">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- ======================================================= -->
                <li>Schedules...</li>
                <li>Templates...</li>
                <li>Compaigns...</li>
                <li>Reports...</li>
            </ul>

            <div id="dialog-temp-container" style="display:none1">
                <div id="modal-container" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body" style="width:1000px;">
                        <h2 id="modal-header" class="uk-modal-title">New Group</h2>
                        <form id="form-edit-group" class="uk-form-stacked">
                            <input type="hidden" id="group-id" name="group_id" value="-1" />
                            <div class="uk-form-row uk-margin-top">
                                <label class="uk-form-label" for="form-stacked-text">Group Name*:</label>
                                <div class="uk-form-controls">
                                    <input id="group_name" name="group_name" class="uk-input uk-form-width-large"  type="text" />
                                </div>
                            </div>
                            <div class="uk-form-row uk-margin-top">
                                <label class="uk-form-label" for="form-stacked-text">Description*:</label>
                                <div class="uk-form-controls">
                                    <input id="group_description" name="group_description" class="uk-input"  type="text" />
                                </div>
                            </div>
                            <hr/>
                            <div class="uk-form-row uk-margin-top">
                                <div style="display:inline-block;width:70%;">
                                    Add user information and click "Add" button to add user members.
                                </div>
                                <div style="display:inline-block;width:28%;text-align:right;">
                                    <a href="#" onclick="console.log('add from users.');return false;">Add from Users</a>
                                    <a href="#" style="margin-left:10px;" onclick="console.log('add from csv.');return false;">Add from CSV</a>
                                </div>
                            </div>
                            <div class="uk-form-row uk-margin-top">
                                <div style="display:inline-block;width:35%;">
                                    <label class="uk-form-label" for="form-stacked-text">Full Name*:</label>
                                    <div class="uk-form-controls">
                                        <input id="user-name" class="uk-input"  type="text" />
                                    </div>
                                </div>
                                <div style="display:inline-block;width:30%;">
                                    <label class="uk-form-label" for="form-stacked-text">Email Address*:</label>
                                    <div class="uk-form-controls">
                                        <input id="user-email" class="uk-input"  type="text" />
                                    </div>
                                </div>
                                <div style="display:inline-block;width:20%;">
                                    <label class="uk-form-label" for="form-stacked-text">Country*:</label>
                                    <div class="uk-form-controls">
                                        <select id="user-country" class="uk-select">
                                        </select>
                                    </div>
                                </div>
                                <div style="display:inline-block;width:10%;">
                                    <button type="button" class="uk-button uk-button-default" onclick="submit_new_user();return false;" >Add</button>
                                </div>
                            </div>

                            <div class="uk-form-row uk-margin-top">
                                <table id="users-table" class="uk-table uk-table-hover uk-table-divider">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center;"><span uk-icon="icon: trash"></span></th>
                                            <th style="text-align:center;"><span uk-icon="icon: cog"></span></th>
                                            <th>Full Name</th>
                                            <th>Email Address</th>
                                            <th>Country</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                        </form>
                        <p class="uk-text-right">
                            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                            <button class="uk-button uk-button-primary" type="button" onclick="save_group();return false;">Save</button>
                        </p>
                    </div>
                </div>
            </div>

        </div>

    </body>
</html>
